///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about lists
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @todo 03_27_21
///////////////////////////////////////////////////////////////////////////////
#include "animal.hpp"
#include "list.hpp"
//#include "node.hpp"
#include <cstddef>

using namespace std;

namespace animalfarm{
const bool SingleLinkedList::empty() const{
  
   if (head == nullptr)
      return true;
   return false;
}

void SingleLinkedList::push_front(Node* newNode){
   newNode -> next = head;
   head = newNode;
}

Node* SingleLinkedList::pop_front(){
   if(head != nullptr){
   Node* temp = head;
   head = temp-> next;
   //free (temp);
   return temp;
   }
   else
      return nullptr;
}

Node* SingleLinkedList::get_first() const{
   return head;

}

Node* SingleLinkedList::get_next(const Node* currentNode) const{
   Node* node = head;
   while(node != currentNode){
      node = node-> next;
   }
   return node -> next;
}

size_t SingleLinkedList::size() const{
   size_t i = 0;
   Node* node = head;
   while(node != nullptr){
      i++;
      node = node-> next;
   }
   return i;
}

} //namespace animalfarm
