///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file node.hpp
/// @version 1.0
///
/// Exports data about nodes
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @todo 03_27_21
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace animalfarm{

class Node{
   friend class SingleLinkedList;
   protected:  
      Node* next = nullptr;
};

} //namespace animalfarm
