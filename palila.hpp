///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @03_05_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "bird.hpp"

namespace animalfarm {

class Palila : public Bird {
public:
	Palila( string newWhereFound, enum Color newColor, enum Gender newGender );

	string whereFound;
	
	void printInfo();
};

} // namespace animalfarm

