###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Ariel Thepsenavong <arielat@hawaii.edu>
# @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
# @date   @03_26_2021
###############################################################################

all: main

#test: test.cpp
#	g++ -o test testmain.cpp

#test.o: test.cpp node.hpp
#	g++ -c test.cpp

node.o: node.cpp node.hpp
	g++ -c node.cpp

list.o: list.cpp list.hpp
	g++ -c list.cpp

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

bird.o: bird.hpp bird.cpp
	g++ -c bird.cpp

fish.o: fish.hpp fish.cpp
	g++ -c fish.cpp
	
nunu.o: nunu.hpp nunu.cpp
	g++ -c nunu.cpp
	
aku.o: aku.hpp aku.cpp
	g++ -c aku.cpp
	
nene.o: nene.hpp nene.cpp
	g++ -c nene.cpp
	
palila.o: palila.hpp palila.cpp
	g++ -c palila.cpp
	
dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp
	
cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

animalFactory.o: animalFactory.cpp *.hpp
	g++ -c animalFactory.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalFactory.o node.o list.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o animalFactory.o node.o list.o

#test: test.cpp *.hpp test.o node.o list.o
#	g++ -o test test.o node.o list.o

clean:
	rm -f *.o main
