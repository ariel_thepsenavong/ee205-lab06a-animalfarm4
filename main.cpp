///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @03_27_2021
///////////////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <iostream>
#include <array>
#include <random>
#include <memory>
#include <ctime>
#include <list>
#include "animal.hpp"
#include "animalFactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"


using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 4" << endl;///////////////////////////////                //////////////////////////////////////////////////////////////  Animal List   //////////////////////////////////////////////////////////////                ///////////////////////////////

   SingleLinkedList animalList;  // Instantiate a SingleLinkedList
   for( auto i = 0 ; i < 25 ; i++ ) {
	   animalList.push_front( AnimalFactory::getRandomAnimal() );
	   }
	cout << endl;
	cout << "List of Animals" << endl;
	cout << "  Is it empty: " << boolalpha << animalList.empty() << endl;
	cout << "  Number of elements: " << animalList.size() << endl;
	for( auto animal = animalList.get_first()        // for() initialize   
	; animal != nullptr                           // for() test   
	; animal = animalList.get_next( animal )) {   // for() increment
	cout << ((Animal*)animal)->speak() << endl;
	}
	while( !animalList.empty() ) {
		Animal* animal = (Animal*) animalList.pop_front();
		delete animal;
		}
}

