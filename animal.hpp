///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo 03_05_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"

using namespace std;

#include <string>

namespace animalfarm {

enum Color { BLACK, WHITE, RED, BLUE, GREEN, PINK, SILVER, YELLOW, BROWN, UNKNOWN_COLOR };
enum Gender{MALE, FEMALE, UNKNOWN};

class Animal : public Node{
public:
	enum Gender gender;
	string      species;
   Animal();
   ~Animal();
	virtual const string speak() = 0;
	
	void printInfo();
	
   static const string colorName  (enum Color color) ;
	static const string genderName (enum Gender gender) ;

//	enum Gender{MALE, FEMALE, UNKNOWN_GENDER};
  // enum Color{RED, BLACK, WHITE, SILVER, YELLOW, BROWN, UNKNOWN_COLOR};
   static const string getRandomName();
   static const Gender getRandomGender();
   static const Color getRandomColor();
   static const float getRandomWeight(const float from, const float to);
   static const bool getRandomBool();

};


} // namespace animalfarm
