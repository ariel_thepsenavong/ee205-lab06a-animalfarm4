//TEST

#include <iostream>
#include "node.hpp"
#include "list.hpp"

using namespace std;

int main(){
   cout << "Hello World" << endl;
   Node node; //Instantiate a node
   SingleLinkedList list; //Instantiate a SingleLinkedList
   cout << "bool empty function: " << list.empty() << endl;
   list.push_front(&node);
   list.pop_front();
   cout << list.get_first() <<endl;
   cout << list.get_next(&node) << endl;
   return 0;
}
