///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all list
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @todo 03_27_21
///////////////////////////////////////////////////////////////////////////////
#pragma once

class Node;
namespace animalfarm{

class SingleLinkedList{
   protected:
      Node* head = nullptr;
//      Node node;
   public:
      const bool empty() const;  //return true if list is empty, false if not
      void push_front(Node* newNode);  // Add newNode to the front of the list
      Node* pop_front();   //Remove a node from front of list. if list is already empty, return nullptr
      Node* get_first() const;   //return first node
      Node* get_next(const Node* currentNode) const;  //return node immediately after currentNode
      size_t size() const;    // return the number of nodes in list

};

} // namespace animalfarm
