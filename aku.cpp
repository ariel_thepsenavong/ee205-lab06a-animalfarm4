///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm2 - EE 205 - Spr 2021 
/// @date   @03_27_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {
	
Aku::Aku( float newWeight, enum Color newColor, enum Gender newGender ) {
	gender = newGender;
	species = "Katsuwonus pelamis";
	scaleColor = newColor;
	favoriteTemp = 75;
	weight = newWeight;
}


void Aku::printInfo() {
	cout << "Aku" << endl;
	cout << "   Weight = [" << weight << "]" << endl;
	Fish::printInfo();
}

} // namespace animalfarm
