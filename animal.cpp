///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo 03_05_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <random>
#include "animal.hpp"

using namespace std;

namespace animalfarm {
Animal::Animal() {
	cout << "." ;
   }

Animal::~Animal() {
	cout << "x" ;
}
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


const string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	
const string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case BLUE:   return string("Blue"); break;
      case GREEN:  return string("Green"); break;
      case PINK:   return string("Pink"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }

   return string("Unknown");
};
	
const Gender Animal::getRandomGender(){
   srand(time(NULL));
   Gender gender = Gender(rand()%3);

   switch(gender){
      case MALE: return MALE; break;
      case FEMALE: return FEMALE; break;
      case UNKNOWN: return UNKNOWN; break;
   } 

   return UNKNOWN;
};

const Color Animal::getRandomColor(){
   srand(time(NULL));
   Color color = Color(rand()%6);

   switch(color){
      case RED: return RED; break;
      case BLACK: return BLACK; break;
      case WHITE: return WHITE; break;
      case BLUE: return BLUE; break; 
      case BROWN: return BROWN; break;
      case GREEN: return GREEN; break;
      case PINK: return PINK; break;
      case SILVER: return SILVER; break;
      case YELLOW: return YELLOW; break;
   } 

   return UNKNOWN_COLOR;
};

const bool Animal::getRandomBool(){
   srand(time(NULL));
   bool pick = rand()%2;

   switch(pick){
      case 0: cout << "true" <<endl; break;
      case 1: cout << "false" <<endl ; break;
     
   } 

   return true;
};

const float Animal::getRandomWeight(const float from, const float to){
   srand(time(NULL));
   const float range = to - from;
   float random = range * ((((float) rand()) / (float) RAND_MAX)) + from;
   return random;


   
}

const string Animal::getRandomName(){
   char randomName[9];
   srand(time(NULL));
   int length = rand() % 6 + 4;  //random length from 4-9
   char upperC = 65 + rand()%26;
   randomName[0] = upperC;

   for(int i = 1; i< length; i++){
      randomName[i] = 97 +rand()%26;
   }

   return "";
};

} // namespace animalfarm
